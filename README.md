# Chat Application

> A Vue.js project by Erik Thiele and Hanna Wagner

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# Install GraphQL Dependencies
cd graphql
yarn install

# run on localhost:3000/graphiql
cd graphql
yarn start

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```
