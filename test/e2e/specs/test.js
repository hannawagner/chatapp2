// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'default e2e tests': function test(browser) {
    const devServer = browser.globals.devServerURL;

    browser
      .url(devServer)
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('#navigate')
      .assert.containsText('h1', 'Please enter your Name to get started!')
      .assert.elementCount('button', 2)
      .end();
  },
};
