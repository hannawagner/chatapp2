import Vue from 'vue';
import EnterPage from '@/components/EnterPage';
import SecondPage from '@/components/SecondPage';

describe('EnterPage.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(EnterPage);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('h1').textContent)
      .toEqual('Please enter your Name to get started!');
  });
});

describe('SecondPage.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(SecondPage);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('h1').textContent)
      .toEqual('Overview');
  });
});

