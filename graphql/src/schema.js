import {makeExecutableSchema} from 'graphql-tools';
import typeDefs from './types.graphql';
import users from '../data/users';
import messages from '../data/messages';

//start counter
let UserId = 3;
let MessageId = 3;

const resolvers = {
  Query: {
    // user: (id) => users[0],
    user(id, info, o) {
      return users[info.id];
    },
    totalUsers: () => users.length,
    allUsers: () => users,

    totalMessages: () => messages.length,
    message(id, info, o) {
      return messages[info.id];
    },
    allMessages: () => messages,


  },
  Mutation: {
    createUser: (root, args) => {
      const newUser = {
        id: UserId++, first_name: args.first_name, last_name: args.last_name
      };
      users.push(newUser);
      return newUser;
    },
    createMessage: (root, args) => {
      const newMessage = {
        id: MessageId++, username: args.username, content: args.content
      };
      messages.push(newMessage);
      return newMessage;
    }
  },

};

export default makeExecutableSchema({typeDefs, resolvers});
