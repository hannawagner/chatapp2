import express from 'express';
import bodyParser from 'body-parser';
import {graphqlExpress, graphiqlExpress} from 'apollo-server-express';
import schema from './schema';

const app = express();
const cors = require('cors');

const port = 3000;

const corsOpt = {
  origin(origin, callback) {
    callback(null, true);
  },
  credentials: true,
};
app.use(cors(corsOpt));
const crossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type,token');
  next();
};


app.use(crossDomain);

app.use('/graphql', bodyParser.json(), graphqlExpress({schema}));
app.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
}));
app.listen(port, () => console.log(`Server on ${port}`));
